#include <stdio.h>
#include <stdlib.h>
#define SIZE 16
#include "task3_2.h"

extern LIST *listMain;
extern BUF *bufMain;
extern int c;
char specialSymbol[] = {'\n', ' ', '\t', '>', '<', '&', '|', ';', '(', ')'};
char allowSymbol[] = {'/', '.', '$', '_'};


/* Функции для работы со списком */
void clearlist(){
    int i;
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    if ((listMain -> lst)==NULL) return;
    for (i=0; (listMain -> lst)[i]!=NULL; i++)
        free((listMain -> lst)[i]);
    free((listMain -> lst));
    (listMain -> lst)=NULL;
}

void nulllist(){
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    (listMain -> lst)=NULL;
}

void termlist(){
    if ((listMain -> lst)==NULL) return;
    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)+1)*sizeof(*(listMain -> lst)));
    (listMain -> lst)[(listMain -> curlist)]=NULL;
    (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)=(listMain -> curlist)+1)*sizeof(*(listMain -> lst)));
}

void printlist(){
    int i;
    if ((listMain -> lst)==NULL) return;
    for (i=0; i<(listMain -> sizelist)-1; i++)
    printf("%s\n",(listMain -> lst)[i]);
    printf("--------------------\n");
}

int compare(char *a, char *b){
    int i = -1;
    while (1){
        i++;
        if (a[i]>b[i]) {
            return 1;
        }
        if (a[i]<b[i]) {
            return 0;
        }
        if ((a[i]=='\0') && (b[i]!='\0')){
            return 0;
        }
        if ((a[i]!='\0') && (b[i]=='\0')){
            return 1;
        }
        if ((a[i]=='\0') && (b[i]=='\0')){
            return 1;
        }
    }

}

void sortlist(){
    int i,j;
    char *S;
    if ((listMain -> lst)==NULL) return;
    for (i=0; i<(listMain -> sizelist)-1; i++)
        for (j=0; j<i; j++){
             if (compare((listMain -> lst)[j], (listMain -> lst)[i])){
                S = (listMain -> lst)[i];
                (listMain -> lst)[i] = (listMain -> lst)[j];
                (listMain -> lst)[j] = S;
            } 
        }
}

/* Функции для обратоки слов */

int in(char A[], int b, int len){
    for (int i=0; i < len; i++){
        if (A[i] == b) return 1;
    }
    return 0;
}

void check(int elem){
    if (!(((elem >= 'a') && (elem <= 'z')) || ((elem >= 'A') && (elem <= 'Z')) || ((elem >= '0') && (elem <= '9')) || in(specialSymbol, elem, sizeof(specialSymbol)) || in(allowSymbol, elem, sizeof(allowSymbol)) || elem == EOF)){
            termlist();
            clearlist();
            nulllist();
            printf("\nERROR (символ | %c | не разрешён)\n", elem);
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            termlist();
            clearlist();
            nulllist();
            if (c==EOF) {
                stop();
                return;
            }
            c=getchar();
            check(c);
            newline();
        }
}

void nullbuf(){
    (bufMain -> buf)=NULL;
    (bufMain -> sizebuf)=0;
    (bufMain -> curbuf)=0;
}

void addsym(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=SIZE);
    (bufMain -> buf)[(bufMain -> curbuf)++]=c;
}

void addword(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=1);
    (bufMain -> buf)[(bufMain -> curbuf)++]='\0';
    (bufMain -> buf)=realloc((bufMain -> buf),(bufMain -> sizebuf)=(bufMain -> curbuf));

    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst), ((listMain -> sizelist)+=SIZE)*sizeof(*(listMain -> lst))); 
    (listMain -> lst)[(listMain -> curlist)++]=(bufMain -> buf);
}
int symset(int c){
    
    return !in(specialSymbol, c, sizeof(specialSymbol)); 
}
void start() {
    if(c==' '|| c=='\t') { 
        c=getchar();
        check(c); 
        start();
    }
    else if (c==EOF)
    {
        termlist();
        if (listMain -> sizelist)
            printf("%d\n", (listMain -> sizelist)-1);
        printlist();
        sortlist();
        printlist();
        clearlist();
        stop();
    }
    else if (c=='\n') {
        termlist();
        if (listMain -> sizelist)
            printf("%d\n", (listMain -> sizelist)-1);
        printlist();
        sortlist();
        printlist();
        clearlist();
        c=getchar();
        check(c);
        newline();
    }
    else if ((c=='<') || (c=='(') || (c==')') || (c==';')) {
        nullbuf();
        addsym();
        c=getchar();
        check(c);
        greater(' ');
    }
    else if (c=='&') {
        nullbuf();
        addsym();
        c=getchar();
        check(c);
        greater('&');
    }
    else if (c=='|') {
        nullbuf();
        addsym();
        c=getchar();
        check(c);
        greater('|');
    }
    
    else
    { 
        char cprev=c;
        nullbuf();
        addsym();
        c=getchar();
        check(c);
        (cprev=='>')? greater('>'): word();
    }
}

void word(){
    if(symset(c)) {
        addsym();
        c=getchar();
        check(c);
        word();
    }
    else
    {
        addword();
        start();
    }
}

void greater(char l) {

    if((c == l) && (l != ' ')) {
        addsym();
        c=getchar();
        check(c);
        greater2();
    }
    else
    {
        addword();
        start();
    }
}

void greater2() {
    addword();
    start();
}

void newline(){
    clearlist();
    start();
}

void stop(){
    clearlist();
    //nulllist();
    free(listMain);
    free(bufMain);
    exit(0);
}
