#ifndef task3_2
#define task3_2

#include <stdio.h>
#include <stdlib.h>
#define SIZE 16
typedef struct LIST {
                        char **lst;
                        int sizelist;
                        int curlist;
                    }
        LIST;

typedef struct BUF {
                        char *buf;
                        int curbuf;
                        int sizebuf;
                    }
        BUF;

int c;
LIST *listMain;
BUF *bufMain;

void clearlist();
void nulllist();
void termlist();
void printlist();
int compare(char *a, char *b);
void sortlist();
int in(char A[], int b, int len);
void check(int c);
void nullbuf();
void addsym();
void addword();
int symset(int c);
void start();
void word();
void greater(char l);
void greater2();
void newline();
void stop();

#endif