#include <stdio.h>
#include <stdlib.h>
#include "task3_2.h"
#define SIZE 16

extern LIST *listMain;
extern BUF *bufMain;
extern int c;

int main(){
    listMain = (LIST *)malloc(sizeof(LIST));
    bufMain = (BUF *)malloc(sizeof(BUF));
    nulllist();
    nullbuf();
    c=getchar();
    check(c);
    //nulllist();
    start();
}