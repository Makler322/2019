#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <semaphore.h>
#include <sys/shm.h>
#include <pthread.h> 

#define N 5 
#define THINKING 0 
#define HUNGRY 1 
#define EATING 2 
#define LEFT (i - 1) % N 
#define RIGHT (i + 1) % N 

int state[N]; 
int phil[N] = { 0, 1, 2, 3, 4 }; 

sem_t mutex; 
sem_t S[N]; 

void test(int i) 
{ 
	if (state[i] == HUNGRY && state[LEFT] != EATING && state[RIGHT] != EATING) { 
		state[i] = EATING; 
		sleep(2);  
		printf("Философ %d кушает\n", i + 1); 
		sem_post(&S[i]); 
	} 
} 

void take_forks(int i) 
{ 
	sem_wait(&mutex); 
	state[i] = HUNGRY; 
	printf("Философ %d голоден\n", i + 1); 
	test(i); 
	sem_post(&mutex); 
	sem_wait(&S[i]); 
	sleep(1); 
} 

 
void put_forks(int i) 
{ 
	sem_wait(&mutex); 
	state[i] = THINKING;  
	printf("Философ %d покушал и теперь думает\n", i + 1); 
	test(LEFT); 
	test(RIGHT); 
	sem_post(&mutex); 
} 

void* philospher(void* num) 
{ 
	while (1) { 
		int* i = num; 
		sleep(1); 
		take_forks(*i); 
		sleep(0); 
		put_forks(*i); 
	} 
} 

int main() 
{ 
	int i; 
	pthread_t thread_id[N]; 
	sem_init(&mutex, 0, 1); 
	for (i = 0; i < N; i++) 
		sem_init(&S[i], 0, 0); 

	for (i = 0; i < N; i++) { 
		pthread_create(&thread_id[i], NULL, philospher, &phil[i]); 
		printf("Философ %d думает\n", i + 1); 
	} 

	for (i = 0; i < N; i++) 
		pthread_join(thread_id[i], NULL); 
} 
