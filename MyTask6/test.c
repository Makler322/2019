#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <semaphore.h>
#include <sys/shm.h>

#define N 5
#define LEFT (i-1) % N
#define RIGHT (i+1) % N

