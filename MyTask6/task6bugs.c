#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <semaphore.h>
#include <sys/shm.h>
#include <pthread.h>

#define N 5
#define LEFT (i-1) % N
#define RIGHT (i+1) % N

int sons[N];
int mutex;
int s;
//char *state;

//sem_t mutex;
//sem_t s[N];
int state[N];


void SigHndlr(int s){
    for (int i = 0; i<N; i++)
        kill(sons[i], SIGKILL);
    //sem_destroy(&mutex);
    /* key_t key = ftok("help.txt", 'S');
    int mutex = semget(key,0,0666);
    semctl(mutex, 1, IPC_RMID);

    key_t key1 = ftok("help1.txt", 'S');
    int s1 = semget(key1,0,0666);
    for (int i = 0;i<N;i++) semctl(s1, i, IPC_RMID);
    
    key_t key2 = ftok("help2.txt", 'S');
    int statestate = semget(key2,0,0666);
    for (int i = 0;i<N;i++) semctl(statestate, i, IPC_RMID);
    */
}


void down(int semdec, int i)
{
    struct sembuf semd;
    perror("f31");
    semd.sem_num = i;
    semd.sem_op = -1;
    //printf("%d\n", semd.sem_op);
    semd.sem_flg = 0;
    perror("f32");
    //semctl(semdec, 0, SETVAL, );
    semop(semdec, &semd, (size_t)0);
    perror("f33");
}

void up(int semdec, int i)
{
    struct sembuf semd;
    semd.sem_num = i;
    semd.sem_op = 1;
    semd.sem_flg = 0;
    semop(semdec, &semd, 1);
}

void Test(int i){
    //printf("%c %c %c %c %c\n",state[0], state[1], state[2], state[3], state[4]);
    if ((state[i] == '1') && (state[LEFT] != '2') && (state[RIGHT] != '2')){
        //sleep(rand()%3);
        
        state[i] = '2';
        
        //sleep(rand()%3);
        //sem_post(&s[i]);
        up(s, i);
    }
}

void TakeForks(int i){
    //perror("f1");
    //printf("%d - ", (int)mutex);
    //printf("%d", mutex);
    //sem_wait(&mutex);
    down(mutex,0);
    //printf("%d", mutex);

    //perror("f2");
    //printf("%d\n", mutex);
    //sleep(rand()%3);
    //printf("%c - ", state[i]);
    state[i] = '1';
    //perror("f3");
    //printf("%c - \n", state[i]);
    Test(i);
    //perror("f4");
    //sem_post(&mutex);
    up(mutex, 0);
    //perror("f5");
    //sleep(rand()%3);
    //printf("Философ %d покушал и теперь думает\n", i);
    //sem_wait(&s[i]);
    down(s, i);

}

void PutForks(int i){
    //sem_wait(&mutex);
    down(mutex,0);
    
    state[i] = '0';
    Test(LEFT);
    Test(RIGHT);
    //sleep(rand()%3);
    up(mutex,0);
    //sem_post(&mutex);
}



void Philosopher(int i) {
    while (1){
        sleep(2);
        printf("Философ %d думает\n", i);
        sleep(2);
        printf("Философ %d голоден\n", i);
        TakeForks(i);
        printf("Философ %d кушает\n", i);
        sleep(2);
        printf("Философ %d покушал\n", i);
        PutForks(i);
    }
}
/*
void *Philosopher2(void *args) {
    while (1){
        sleep(2);
        printf("Философ %d думает\n", 2);
        sleep(2);
        printf("Философ %d голоден\n", 2);
        TakeForks(2);
        printf("Философ %d кушает\n", 2);
        sleep(2);
        printf("Философ %d покушал\n", 2);
        PutForks(2);
    }
}
void *Philosopher3(void *args) {
    while (1){
        sleep(2);
        printf("Философ %d думает\n", 3);
        sleep(2);
        printf("Философ %d голоден\n", 3);
        TakeForks(3);
        printf("Философ %d кушает\n", 3);
        sleep(2);
        printf("Философ %d покушал\n", 3);
        PutForks(3);
    }
}
void *Philosopher4(void *args) {
    while (1){
        sleep(2);
        printf("Философ %d думает\n", 4);
        sleep(2);
        printf("Философ %d голоден\n", 4);
        TakeForks(4);
        printf("Философ %d кушает\n", 4);
        sleep(2);
        printf("Философ %d покушал\n", 4);
        PutForks(4);
    }
}
void *Philosopher5(void *args) {
    while (1){
        sleep(2);
        printf("Философ %d думает\n", 4);
        sleep(2);
        printf("Философ %d голоден\n", 4);
        TakeForks(0);
        printf("Философ %d кушает\n", 4);
        sleep(2);
        printf("Философ %d покушал\n", 4);
        PutForks(0);
    }
}
*/

int main(){
    
    key_t key;
    char *shmaddr;
    
    key = ftok("help.txt",'S');
    int shmid = shmget(key, N, 0666| IPC_CREAT);
    int *state = shmat(shmid, NULL, 0);
    
    key = ftok("help1.txt",'S');
    int mutex = semget(key, 1, 0666| IPC_CREAT);
    semctl(mutex, 0, SETVAL, (int)1);
    
    key = ftok("help2.txt",'S');
    int s = semget(key, N, 0666| IPC_CREAT);
    for (int i =0; i<N; i++) {
        semctl(s, i, SETVAL, 0);
        }
    
    //sem_init(&mutex, 0, 2);
    //sem_init(&, 0, 2);
    //sem_init(&mutex,0,2);
    signal(SIGINT, SigHndlr);
    for (int i=0; i<N; i++){
        //sleep(1);
        if (fork() == 0){
            sons[i] = getpid();
            //perror("f0");
            Philosopher(i);
            exit(0);
        }
    }
    //sem_destroy(&mutex);
    while(1){}
    
/*
pthread_t thread1;
pthread_t thread2;
pthread_t thread3;
pthread_t thread4;
pthread_t thread5;
srand(time(NULL));

//signal(SIGINT, SigHndlr);

sem_init(&mutex, 0, 2);

/* 
if (fork == 0){
    while (1){
        sleep(2);
        printf("Философ %d думает\n", 1);
        sleep(2);
        printf("Философ %d голоден\n", 1);
        TakeForks(0);
        printf("Философ %d кушает\n", 1);
        sleep(2);
        printf("Философ %d покушал\n", 1);
        PutForks(0);
    }
}
if (fork == 0){
    while (1){
        sleep(2);
        printf("Философ %d думает\n", 2);
        sleep(2);
        printf("Философ %d голоден\n", 2);
        TakeForks(1);
        printf("Философ %d кушает\n", 2);
        sleep(2);
        printf("Философ %d покушал\n", 2);
        PutForks(1);
    }
}
if (fork == 0){
    while (1){
        sleep(2);
        printf("Философ %d думает\n", 3);
        sleep(2);
        printf("Философ %d голоден\n", 3);
        TakeForks(2);
        printf("Философ %d кушает\n", 3);
        sleep(2);
        printf("Философ %d покушал\n", 3);
        PutForks(2);
    }
}
if (fork == 0){
    while (1){
        sleep(2);
        printf("Философ %d думает\n", 4);
        sleep(2);
        printf("Философ %d голоден\n", 4);
        TakeForks(3);
        printf("Философ %d кушает\n", 4);
        sleep(2);
        printf("Философ %d покушал\n", 4);
        PutForks(3);
    }
}9
if (fork == 0){
    while (1){
        sleep(2);
        printf("Философ %d думает\n", 5);
        sleep(2);
        printf("Философ %d голоден\n", 5);
        TakeForks(4);
        printf("Философ %d кушает\n", 5);
        sleep(2);
        printf("Философ %d покушал\n", 5);
        PutForks(4);
    }
}
while(1){}
 
pthread_create(&thread1, NULL, Philosopher1, NULL);
pthread_create(&thread2, NULL, Philosopher2, NULL);
pthread_create(&thread3, NULL, Philosopher3, NULL);
pthread_create(&thread4, NULL, Philosopher4, NULL);
pthread_create(&thread5, NULL, Philosopher5, NULL);
pthread_join(thread1, NULL);
pthread_join(thread2, NULL);
pthread_join(thread3, NULL);
pthread_join(thread4, NULL);
pthread_join(thread5, NULL);
sem_destroy(&mutex);
*/
}
