#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <pwd.h>
#include <fcntl.h>
#include <grp.h>

int main(int argc, char **argv){
    int countSTR = 1, countELEM = 1;
    FILE *fd1, *fd2;
    fd1 = fopen(argv[1], "r");
    fd2 = fopen(argv[2], "r");
    if ((fd1 == NULL) || (fd2 == NULL)){
        printf("Ошибка при открытии\n");
        exit(0);
    }
    char c1, c2;
    int flag=1;
    
    while(((c1 = fgetc(fd1)) != EOF) && ((c2 = fgetc(fd2)) != EOF)){
        if (c1 != c2){
            printf("%s differs from %s: line %d char %d\n", argv[1], argv[2], countSTR, countELEM);
            flag = 0;
            break;
        }
        else{
            if (c1 == '\n'){
                countSTR += 1;
                countELEM = 1;
            }
            else {
                countELEM += 1;
            }
        }
    }
    if (flag){
        printf("No differs\n");
    }
    fclose(fd1);
    fclose(fd2); 
}