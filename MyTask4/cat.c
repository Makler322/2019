#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <pwd.h>
#include <fcntl.h>
#include <grp.h>

int main(int argc, char **argv){
    int i=1, flagn = 0, count = 1;
    for (; i < argc; i++){
        if (!strcmp("-n", argv[i])){
            flagn = 1;
            continue;
        }
        else if ('-' == argv[i][0]){
            printf("Флаг неверный\n");
            continue;
        }
        break;
    }
    for (; i < argc; i++){
            FILE *fd1;
            fd1 = fopen(argv[i], "r");
            char c;
            int flag=0;
            if (flagn){
                printf("%d) ", count);
                while((c = fgetc(fd1)) != EOF){
                    if (flag){
                        printf("%d) ", count);
                        flag = 0;
                    }
                    if (c == '\n'){
                        printf("%c", c);
                        count++;
                        flag = 1;
                    }
                    else printf("%c", c);
                }
            }
            else{
                while((c = fgetc(fd1)) != EOF){
                    printf("%c", c);
                }
            }
            fclose(fd1); 
        }
}