#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <pwd.h>
#include <fcntl.h>
#include <grp.h>

int isItDirectory(char *name){
    int n = 1;
    char *buff = NULL;
    buff=(char*)malloc(n);
    while (!getcwd(buff, n)){
        free(buff);
        n+=1;
        buff=(char*)malloc(n);
    }
    char *fullPath1 = malloc(strlen(buff)+strlen(name)+2);
    strcpy(fullPath1, buff);
    strcat(fullPath1, "/");
    strcat(fullPath1, name);
    //printf("%s\n", fullPath1);
    struct stat buffstat;
    stat(fullPath1, &buffstat);
    int stats = buffstat.st_mode;
    //printf("%d", stats);
    
    if (stats & S_IFDIR) return 1; else return 0;
}


int main(int argc, char **argv){
    int n = 1;
    char *buff=NULL;
    FILE *fd1, *fd2;

    if (!strcmp(argv[1],argv[2])){
        printf("Одинаковые файлы\n");
        exit(0);
    }


    buff=(char*)malloc(n);
    while (!getcwd(buff, n)){
        free(buff);
        n+=1;
        buff=(char*)malloc(n);
    }
    if (isItDirectory(argv[2])){
        char *fullPath13 = malloc(strlen(buff)+strlen(argv[2])+strlen(argv[2])+3);
        strcpy(fullPath13, buff);
        strcat(fullPath13, "/");
        strcat(fullPath13, argv[2]);
        strcat(fullPath13, "/");
        strcat(fullPath13, argv[1]);
        fd2 = fopen(fullPath13, "w+");
    }
    else{
        fd2 = fopen(argv[2], "w");
    }
    fd1 = fopen(argv[1], "r");
    
    if ((fd1 == NULL)|| (fd2 == NULL)){
        printf("Ошибка при открытии \n");
        exit(0);
    }

    char c1;
    
    while(((c1 = fgetc(fd1)) != EOF)){
        fputc(c1, fd2);
    }
    fclose(fd1);
    fclose(fd2); 
}