#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <pwd.h>
#include <fcntl.h>
#include <grp.h>

int isItDirectory(char *name){
    int n = 1;
    char *buff = NULL;
    buff=(char*)malloc(n);
    while (!getcwd(buff, n)){
        free(buff);
        n+=1;
        buff=(char*)malloc(n);
    }
    char *fullPath1 = malloc(strlen(buff)+strlen(name)+2);
    strcpy(fullPath1, buff);
    strcat(fullPath1, "/");
    strcat(fullPath1, name);
    struct stat buffstat;
    stat(fullPath1, &buffstat);
    int stats = buffstat.st_mode;
    
    if (stats & S_IFDIR) return 1; else return 0;
}

void forg(char *name){
    struct stat buffstat;
    struct passwd *pwd;
    stat(name, &buffstat);
    
    pwd = getpwuid(buffstat.st_gid);
    printf("Имя владельца группы: %s\n\n", pwd->pw_name);
    
}

void forl(char *name){
    struct stat buffstat;
    struct passwd *pwd;
    stat(name, &buffstat);
    int stats = buffstat.st_mode;
    printf("Права файла: %d  ", stats);
    if (stats & S_IFDIR) printf("%c", 'd'); else printf("-");
    if (stats & 256) printf("%c", 'r'); else printf("-");
    if (stats & 128) printf("%c", 'w'); else printf("-");
    if (stats & 64) printf("%c", 'x'); else printf("-");
    if (stats & 32) printf("%c", 'r'); else printf("-");
    if (stats & 16) printf("%c", 'w'); else printf("-");
    if (stats & 8) printf("%c", 'x'); else printf("-");
    if (stats & 4) printf("%c", 'r'); else printf("-");
    if (stats & 2) printf("%c", 'w'); else printf("-");
    if (stats & 1) printf("%c", 'x'); else printf("-");
    printf("\n");
    pwd = getpwuid(buffstat.st_uid);
    printf("Имя владельца: %s\n", pwd->pw_name);
    printf("Размер файла: %ld\n", buffstat.st_size);
    printf("\n");
}
void forR(char *name, char *buff, int flagl, int flagg){
    DIR *dir;
    struct dirent *entry;
    char *fullPath = malloc(strlen(buff)+strlen(name)+2);
    strcpy(fullPath, buff);
    strcat(fullPath, "/");
    strcat(fullPath, name);
    if ((dir = opendir(fullPath))) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_name[0] == '.') continue;
            char *fullPath2 = malloc(strlen(fullPath)+strlen(entry->d_name)+2);
            strcpy(fullPath2, fullPath);
            strcat(fullPath2, "/");
            strcat(fullPath2, entry->d_name);
            printf("Имя файла: %s/%s\n",name,entry->d_name);
            if (flagg) forg(fullPath2);
            if (flagl) forl(fullPath2);
            forR(entry->d_name, fullPath2, flagl, flagg);
        }
        closedir(dir);
    }
    free(fullPath);
}

int main(int argc, char **argv) {
    DIR *dir;
    struct dirent *entry;
    int n = 1, i = 1, flagR = 0, flagl = 0, flagg = 0;
    char *buff=NULL;
    for (; i < argc; i++){
        if (!strcmp("-R", argv[i])){
            flagR = 1;
            continue;
        }
        if (!strcmp("-l", argv[i])){
            flagl = 1;
            continue;
        }
        if (!strcmp("-g", argv[i])){
            flagg = 1;
            continue;
        }
        break;
    }
    buff=(char*)malloc(n);
        while (!getcwd(buff, n)){
            free(buff);
            n+=1;
            buff=(char*)malloc(n);
        }
    if (i == argc){
        dir = opendir(buff);
        if (!dir) {
            printf("Ошибка при открытии\n");
            return 0;
        }
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_name[0] == '.') continue;
            printf("Имя файла: %s \n",entry->d_name);
            if (flagg) forg(entry->d_name);
            if (flagl) forl(entry->d_name);
            if (flagR) forR(entry->d_name, buff, flagl, flagg);
        }
        closedir(dir);
    }
    for (;i < argc; i++){
        if (isItDirectory(argv[i])){
            char *fullPath13 = malloc(strlen(buff)+strlen(argv[i])+2);
            strcpy(fullPath13, buff);
            strcat(fullPath13, "/");
            strcat(fullPath13, argv[i]);

            dir = opendir(fullPath13);
            if (!dir) {
                printf("Ошибка при открытии\n");
                return 0;
            }
            //printf("%s\n", fullPath13);
            while ((entry = readdir(dir)) != NULL) {
                if (entry->d_name[0] == '.') continue;
                printf("Имя файла: %s \n",entry->d_name);
                char *fullPath11 = malloc(strlen(fullPath13)+strlen(entry->d_name)+2);
                strcpy(fullPath11, fullPath13);
                strcat(fullPath11, "/");
                strcat(fullPath11, entry->d_name);
                if (flagg) forg(fullPath11);
                if (flagl) forl(fullPath11);
                if (flagR) forR(fullPath11, fullPath13, flagl, flagg);
            }
            closedir(dir);
        }
        else{
            printf("Имя файла: %s \n",argv[i]);
                if (flagg) forg(argv[i]);
                if (flagl) forl(argv[i]);
        }
    }
}