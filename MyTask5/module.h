
#include <stdio.h>
#include <stdlib.h>
#define SIZE 16
typedef struct LIST {
                        char **lst;
                        int sizelist;
                        int curlist;
                    }
        LIST;

typedef struct BUF {
                        char *buf;
                        int curbuf;
                        int sizebuf;
                    }
        BUF;
//char specialSymbol[] = {'\n', ' ', '\t', '>', '<', '&', '|', ';', '(', ')'};

typedef struct cmd_inf {
    char ** argv; // список из имени команды и аргументов
    char *infile; // переназначенный файл стандартного ввода
    char *outfile; // переназначенный файл стандартного вывода
    char *outfileEND;
    int backgrnd; // =1, если команда подлежит выполнению в фоновом режиме
    struct cmd_inf *pipe; // следующая команда после “|”
    struct cmd_inf *next; // следующая после “;” (или после “&”)
} cmd_inf;


int c, countBG;
LIST *listMain;
BUF *bufMain;
cmd_inf *cmdMain;

//moduleA
int testBG();
int testCD();
void fromListToCmd(int i, cmd_inf *cmd);
void runCommand(cmd_inf *cmd);
void conveer(cmd_inf *cmd);
void posledovatelno(cmd_inf *cmd);
void phandler(int ggwp);
char *fromInttoStr(int n);

//moduleB
void printInvite();
void nullcmd();
void clearbuf();
void clearspecial();
void nullspecial();
void clearlist();
void nulllist();
void termlist();
void printlist();
int compare(char *a, char *b);
void sortlist();
int in(char A[], int b, int len);
void check(int c);
void nullbuf();
void addsym();
void addword();
int symset(int c);
void start();
void word();
void greater(char l);
void greater2();
void newline();
void stop();
