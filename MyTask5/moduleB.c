#define SIZE 16
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include "module.h"

extern LIST *listMain;
extern BUF *bufMain;
extern int c;
char specialSymbol[] = {'\n', ' ', '\t', '>', '<', '&', '|', ';', '(', ')'};

char allowSymbol[] = {'/', '.', '$', '_', '-', '#', '"','\'', '\\', '~', ':'};


void clearlist(){
    int i;
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    if ((listMain -> lst)==NULL) return;
    for (i=0; (listMain -> lst)[i]!=NULL; i++)
        free((listMain -> lst)[i]);
    free((listMain -> lst));
    (listMain -> lst)=NULL;
}


void nullbuf(){
    (bufMain -> buf)=NULL;
    (bufMain -> sizebuf)=0;
    (bufMain -> curbuf)=0;
}

void clearcmd(cmd_inf *cmdMain){
    cmdMain -> backgrnd = 0;
    if (cmdMain -> argv)
        for (int i = 0; (cmdMain -> argv)[i] != NULL; i++)
            free((cmdMain -> argv)[i]);
    if (cmdMain -> infile)
        free(cmdMain -> infile);
    if (cmdMain -> outfile)
        free(cmdMain -> outfile);
    if (cmdMain -> outfileEND)
        free(cmdMain -> outfileEND);
    if (cmdMain -> pipe)
        free(cmdMain -> pipe);
    cmdMain -> argv = NULL;
    cmdMain -> infile = NULL;
    cmdMain -> outfile = NULL;
    cmdMain -> outfileEND = NULL;
    cmdMain -> pipe = NULL;
    cmdMain -> backgrnd = 0;
}

void nullcmd(cmd_inf *cmdMain){
    (cmdMain -> argv)=NULL;
    (cmdMain -> infile)=NULL;
    (cmdMain -> outfile)=NULL;
    (cmdMain -> outfileEND)=NULL;
    (cmdMain -> pipe)=NULL;
    (cmdMain -> backgrnd)=0;
}

void nulllist(){
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    (listMain -> lst)=NULL;
}

void termlist(){
    if ((listMain -> lst)==NULL) return;
    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)+1)*sizeof(*(listMain -> lst)));
    (listMain -> lst)[(listMain -> curlist)]=NULL;
    (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)=(listMain -> curlist)+1)*sizeof(*(listMain -> lst)));
}

void printlist(){
    int i;
    if ((listMain -> lst)==NULL) return;
    for (i=0; i<(listMain -> sizelist)-1; i++)
        printf("%s\n",(listMain -> lst)[i]);
    printf("--------------------\n");
}

void printInvite(){
    char dollar = '$';
    write(1, &dollar, 1);
}

int compare(char *a, char *b){
    int i = -1;
    while (1){
        i++;
        if (a[i]>b[i]) {
            return 1;
        }
        if (a[i]<b[i]) {
            return 0;
        }
        if ((a[i]=='\0') && (b[i]!='\0')){
            return 0;
        }
        if ((a[i]!='\0') && (b[i]=='\0')){
            return 1;
        }
        if ((a[i]=='\0') && (b[i]=='\0')){
            return 1;
        }
    }

}

void sortlist(){
    int i,j;
    char *S;
    if ((listMain -> lst)==NULL) return;
    for (i=0; i<(listMain -> sizelist)-1; i++)
        for (j=0; j<i; j++){
             if (compare((listMain -> lst)[j], (listMain -> lst)[i])){
                S = (listMain -> lst)[i];
                (listMain -> lst)[i] = (listMain -> lst)[j];
                (listMain -> lst)[j] = S;
            } 
        }
}


int in(char A[], int b, int len){
    for (int i=0; i < len; i++){
        if (A[i] == b) return 1;
    }
    return 0;
}

void check(int elem){
    if (!(((elem >= 'a') && (elem <= 'z')) || ((elem >= 'A') && (elem <= 'Z')) || ((elem >= '0') && (elem <= '9')) || in(specialSymbol, elem, sizeof(specialSymbol)) ||  in(allowSymbol, elem, sizeof(allowSymbol)) || elem == EOF)){
        printf("\nERROR (символ | %c | не разрешён)\n", elem);
        while (1){
            c=getchar();
            if ((c == '\n') || (c == EOF)) break; 
        }
        
        if (c==EOF){
            stop();
        }
            
        
        clearlist();
        char dollar = '$';
        write(1, &dollar, 1);
        c=getchar();
        check(c);
        newline();   
    }

}


void addsym(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=SIZE);
    (bufMain -> buf)[(bufMain -> curbuf)++]=c;
}

void addword(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=1);
    (bufMain -> buf)[(bufMain -> curbuf)++]='\0';
    (bufMain -> buf)=realloc((bufMain -> buf),(bufMain -> sizebuf)=(bufMain -> curbuf));

    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst), ((listMain -> sizelist)+=SIZE)*sizeof(*(listMain -> lst))); 
    (listMain -> lst)[(listMain -> curlist)++]=(bufMain -> buf);
        
}
int symset(int c){
    return !in(specialSymbol, c, sizeof(specialSymbol)); 
}
void start() {
    
    if(c==' '|| c=='\t') { 
        c=getchar();
        check(c); 
        start();
    }
    else if (c==EOF){
        termlist();
        fromListToCmd(0, cmdMain);
        if (listMain -> sizelist){
            if (testCD()){
                ////perror("1");
                if (listMain -> sizelist == 2){
                    chdir(getenv("HOME"));

                }
                else if (strcmp((listMain -> lst)[1], "~") == 0){
                    chdir(getenv("HOME"));
                }
                else {
                    chdir((listMain -> lst)[1]);
                }
                ////perror("2");
            }
            else if (cmdMain -> next != NULL){
                posledovatelno(cmdMain);
            }
            else if (cmdMain -> pipe != NULL){
                conveer(cmdMain);
            }
            else runCommand(cmdMain);
        }

        stop();
    }
    else if (c=='\n') {
        termlist();
        ////perror("184");
        //printlist();
        ////perror("185");
        fromListToCmd(0, cmdMain);
        ////perror("186");
        if (listMain -> sizelist){
            if (testCD()){
                ////perror("1");
                if (listMain -> sizelist == 2){
                    chdir(getenv("HOME"));

                }
                else if (strcmp((listMain -> lst)[1], "~") == 0){
                    chdir(getenv("HOME"));
                }
                else {
                    chdir((listMain -> lst)[1]);
                }
                ////perror("2");
            }
            else if (cmdMain -> next != NULL){
                posledovatelno(cmdMain);
            }
            else if (cmdMain -> pipe != NULL){
                conveer(cmdMain);
            }
            else runCommand(cmdMain);
        }
        
        ////perror("187");
        clearcmd(cmdMain);
        clearlist();
        printInvite();
        ////perror("188");
        c=getchar();
        check(c);
        newline();
    }

    else if (c==';' || c=='<') {
        nullbuf();
        addsym();
        c=getchar();
        check(c);
        greater(' ');
    }


    else if (c=='&') {
        nullbuf();
        addsym();
        c=getchar();
        check(c);
        greater('&');
    }
    else if (c=='|') {
        nullbuf();
        addsym();
        c=getchar();
        check(c);
        greater('|');
    }
    else if (c=='"') {
        nullbuf();
        c=getchar();
        while (((c != '"') && (c != '\n') && (c != EOF))){
            addsym();
            c=getchar();
        }

        if (c==EOF){
            printf("\nERROR (нет второй кавычки)\n");
            stop();
        }
            
        else if (c=='\n'){
            clearlist();
            printf("\nERROR (нет второй кавычки)\n");
            printInvite();
            c=getchar();
            check(c);
            newline();
        }
        else {
            c=getchar();
            check(c);
            word();
        }
    }
    else if (c=='\'') {
        nullbuf();
        c=getchar();
        while ((c != '\'') && (c != '\n') && (c != EOF)){
            addsym();
            c=getchar();
        }

        if (c==EOF){
            printf("\nERROR (нет второй кавычки ('))\n");
            stop();
        }
            
        else if (c=='\n'){
            clearlist();
            printf("\nERROR (нет второй кавычки ('))\n");
            
            printInvite();
            c=getchar();
            check(c);
            newline();
        }

        else {
            c=getchar();
            check(c);
            word();
        }
    }
    else if (c == '\\'){
        nullbuf();
        c=getchar();
        word();

    }
    else
    {
        
        char cprev=c;
        nullbuf();
        addsym();
        c=getchar();
        check(c);
        (cprev=='>')? greater('>'): word();
    }
}

void word(){
    if(symset(c)){
        addsym();
        c=getchar();
        check(c);
        word();
    }
    else{
        addword();
        start();
    }
}

void greater(char l) {

    if((c == l) && (l != ' ')) {
        addsym();
        c=getchar();
        check(c);
        greater2();
    }
    else
    {
        addword();
        start();
    }
}

void greater2() {
    addword();
    start();
}

void newline(){
    clearlist();
    start();
}

void stop(){
    clearlist();
    clearcmd(cmdMain);
    free(listMain);
    free(bufMain);
    free(cmdMain);
    exit(0);
}
