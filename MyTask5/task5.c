#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

#include "module.h"

extern LIST *listMain;
extern BUF *bufMain;
extern cmd_inf *cmdMain;
extern int c;

int main(){
    signal(SIGCHLD, phandler);
    listMain = (LIST *)malloc(sizeof(LIST));
    bufMain = (BUF *)malloc(sizeof(BUF));
    cmdMain = (cmd_inf *)malloc(sizeof(cmd_inf));
    nulllist();
    nullbuf();
    nullcmd(cmdMain);
    printInvite();
    c=getchar();
    check(c);
    start();
}