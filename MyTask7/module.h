#define ADDRESS "mysocket" // адрес для связи
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <pwd.h>
#include <sys/socket.h>
#include <sys/un.h>


// Server
int getConnectionServer();
void workServer(int connectionDescriptor);

// Client

int getConnectionClient();
void workClient(int connectionDescriptor);

// Копипаст с task5 

#define SIZE 16



typedef struct LIST {
                        char **lst;
                        int sizelist;
                        int curlist;
                    }
        LIST;

typedef struct BUF {
                        char *buf;
                        int curbuf;
                        int sizebuf;
                    }
        BUF;
//char specialSymbol[] = {'\n', ' ', '\t', '>', '<', '&', '|', ';', '(', ')'};

typedef struct cmd_inf {
    char ** argv; // список из имени команды и аргументов
    char *infile; // переназначенный файл стандартного ввода
    char *outfile; // переназначенный файл стандартного вывода
    char *outfileEND;
    int backgrnd; // =1, если команда подлежит выполнению в фоновом режиме
    struct cmd_inf *pipe; // следующая команда после “|”
    struct cmd_inf *next; // следующая после “;” (или после “&”)
} cmd_inf;

int c, countBG, flagQuit,connectionDescriptor, flagName;
LIST *listMain;
BUF *bufMain;
cmd_inf *cmdMain;
fd_set readfds;
char *message;
//moduleA
void acceptMessage(int connectionDescriptor);
int testBG();
int testCD();
void fromListToCmd(int i);
void runCommand(cmd_inf *cmd);
void conveer(cmd_inf *cmd);
void posledovatelno(cmd_inf *cmd);
void phandler(int ggwp);
char *fromInttoStr(int n);
void deleteFromList(int elem);
void appendToList(int elem);
void clearcmd(cmd_inf *cmdMain);
//moduleB
void printInvite();
void nullcmd();
void clearbuf();
void clearspecial();
void nullspecial();
void clearlist();
void nulllist();
void termlist();
void printlist();
int compare(char *a, char *b);
void sortlist();
int in(char A[], int b, int len);
void check(int c);
void nullbuf();
void addsym();
void addword();
int symset(int c);
void start();
void word();
void greater(char l);
void greater2();
void newline();
void stop();