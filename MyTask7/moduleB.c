#define SIZE 16
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <netinet/in.h>
#include "module.h"

extern LIST *listMain;
extern BUF *bufMain;
extern int c, flagQuit, flagName;
extern fd_set readfds;

void start() { // Суть данной функции - распарсить введённые данные в список слов
    
    if(c==' '|| c=='\t') { 
        c=getchar();
        start();
    }
    else if (c=='\n' || c==EOF || c=='\r') {
        if (flagName){
            flagName = 0;
            termlist();
            free(message);
            message = NULL;
            fromListToCmd(0); // А вот здесь уже работа со словами
            
            struct sockaddr_in sa;
            sa.sin_family = AF_INET;
            sa.sin_port = htons(5000);
            sa.sin_addr.s_addr = htonl(INADDR_ANY);
            if (connect(connectionDescriptor, (struct sockaddr *)&sa, sizeof(sa)) < 0){
                perror("Сервер не активен");
                exit(1);
            }
            fd_set readfds;
            FD_ZERO(&readfds); 
            FD_SET(connectionDescriptor, &readfds);
            
           
            if (message == NULL){
            
                write(connectionDescriptor, "\n", 1);
            }
            else{
         
                write(connectionDescriptor, message, strlen(message));
            }
         
            clearlist();
            
            acceptMessage(connectionDescriptor);
            newline();
       
        }
        else{
            termlist();
            free(message);
            message = NULL;
            fromListToCmd(0);
            
            
            if (message == NULL){
                write(connectionDescriptor, "\n", 1);
            }
            // Эту команду я обрабатываю на клиенте, она не требует подключения к серверу
            else if (!strcmp(message,"\\help")){ 
                write(1, "\\quit - Выход из чата\n", 34);
                write(1, "\\users - Получить список пользователей, которые сейчас онлайн\n", 109);
                write(1, "\\private <nickname> <message> - Отправить приватное сообщение\n", 90);
                write(1, "\\privates - Получить список всех пользователей, с которыми вы состоите в приватных переписках\n", 165);
                write(1, "\\help - Вывести эту справку\n", 46);
            }
            else{
                write(connectionDescriptor, message, strlen(message));
            }
            clearlist();
        
            acceptMessage(connectionDescriptor);
            newline();
        }
    }
    else
    {
        nullbuf();
        addsym();
        c=getchar();
        word();
    }
}
/*------------------ */

void acceptMessage(int connectionDescriptor){
    //Принять сообщения могу либо с клавиатуры (0) либо с сервера (connectionDescriptor)
    char msd;
    FD_SET(connectionDescriptor, &readfds);
    FD_SET(0, &readfds);

    select(connectionDescriptor+1, &readfds, 0, 0, 0);

    if (FD_ISSET(connectionDescriptor, &readfds)){
        
        if (read(connectionDescriptor, &msd, sizeof(char)) != 0){
            if (!msd == '\0') {
                write(1, &msd, sizeof(char));
                acceptMessage(connectionDescriptor);
            }
            
        }
        else{
            write(1, "Сервер закончил свою работу\n", 53);
            stop();
        }
    }
    else if (FD_ISSET(0, &readfds)){
        c = getchar();
    }
}

void fromListToCmd(int i){
    while (i<(listMain->sizelist)-1){
        // Эту команду тоже обрабатываем на клиенте. Зачем загружать сервер, ему и так тяжело :)
        if ((!strcmp((listMain -> lst)[i], "\\quit")) && (i==0)){
            stop();
        }
        if ((!strcmp((listMain -> lst)[i], "\\help")) && (i==0)){
            message = realloc(message, strlen((listMain -> lst)[i])+1);
            strcpy(message, (listMain -> lst)[i]);
            break;
        }
        else {
            // В конец сообщений вставляем \n
            if (message != NULL) {
                if (i ==(listMain->sizelist)-2){
                    message = realloc(message, strlen(message)+strlen((listMain -> lst)[i])+2);
                    strcat(message, (listMain -> lst)[i]);
                    strcat(message, "\n");
                }
                else{
                    message = realloc(message, strlen(message)+strlen((listMain -> lst)[i])+2);
                    strcat(message, (listMain -> lst)[i]);
                    strcat(message, " ");
                }
                
            }
            else{
               
                if (i ==(listMain->sizelist)-2){
                    message = realloc(message, strlen((listMain -> lst)[i])+2);
                    strcpy(message, (listMain -> lst)[i]);
                    strcat(message, "\n");
                }
                else{
                    message = realloc(message, strlen((listMain -> lst)[i])+2);
                    strcpy(message, (listMain -> lst)[i]);
                    strcat(message, " ");
                }
                
            }
            i++;
        }
    }
}



/* Все функции, которые находятся снизу копируюутся из Task3 и Task5. Их суть - граматно распарсить данные в список*/
void clearlist(){
    int i;
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    if ((listMain -> lst)==NULL) return;
    for (i=0; (listMain -> lst)[i]!=NULL; i++)
        free((listMain -> lst)[i]);
    free((listMain -> lst));
    (listMain -> lst)=NULL;
}

void nullbuf(){
    (bufMain -> buf)=NULL;
    (bufMain -> sizebuf)=0;
    (bufMain -> curbuf)=0;
}


void nulllist(){
    (listMain -> sizelist)=0;
    (listMain -> curlist)=0;
    (listMain -> lst)=NULL;
}

void termlist(){
    if ((listMain -> lst)==NULL) return;
    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)+1)*sizeof(*(listMain -> lst)));
    (listMain -> lst)[(listMain -> curlist)]=NULL;
    (listMain -> lst)=realloc((listMain -> lst),((listMain -> sizelist)=(listMain -> curlist)+1)*sizeof(*(listMain -> lst)));
}

void addsym(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=SIZE);
    (bufMain -> buf)[(bufMain -> curbuf)++]=c;
}

void addword(){
    if ((bufMain -> curbuf)>(bufMain -> sizebuf)-1)
        (bufMain -> buf)=realloc((bufMain -> buf), (bufMain -> sizebuf)+=1);
    (bufMain -> buf)[(bufMain -> curbuf)++]='\0';
    (bufMain -> buf)=realloc((bufMain -> buf),(bufMain -> sizebuf)=(bufMain -> curbuf));

    if ((listMain -> curlist)>(listMain -> sizelist)-1)
        (listMain -> lst)=realloc((listMain -> lst), ((listMain -> sizelist)+=SIZE)*sizeof(*(listMain -> lst))); 
    (listMain -> lst)[(listMain -> curlist)++]=(bufMain -> buf);
        
}

void word(){
    if (c != '\n' && c != '\t' && c != ' '){
        addsym();
        c=getchar();
        
        word();
    }
    else{
        addword();
        start();
    }
}

void newline(){
    clearlist();
    start();
}

void stop(){
    clearlist();
    
    free(listMain);
    free(bufMain);
    
    exit(0);
}
