#include <stdio.h>
#include <stdlib.h>



typedef struct WORD { char elem;
                      struct WORD *next;
                    }
        word;

typedef struct TEXT { word *elem;
                      struct TEXT *next;
                    }
        text;


void push_word(word **pilot, char c)
/* Вставить символ c в конец слова */
    {
        if (*pilot == NULL)
        /* Отдельно обрабатываю первый элемент */
            {
                *pilot = (word *)malloc(sizeof(word));
                (*pilot) -> elem = c;
                (*pilot) -> next = NULL;
                return;
            }
        /* Создаю новую ссылку, чтобы уберечь основную */
        word *lp = *pilot;
        while (((lp) -> next) != NULL)
            lp = (lp) -> next;
        lp-> next = (word *)malloc(sizeof(word));
        (lp) -> next -> elem = c;
        (lp) -> next -> next = NULL;
    }

void push_text(text **pilot, word *w)
/* Вставить слово w в конец списка *lp */
    {
        if (*pilot == NULL)
        /* Отдельно обрабатываю первый элемент */
            {
                *pilot = (text *)malloc(sizeof(text));
                (*pilot) -> elem = w;
                (*pilot) -> next = NULL;
                return;
            }
        /* Создаю новую ссылку, чтобы уберечь основную */
        text *lp = *pilot;
        while (((lp) -> next) != NULL)
            lp = (lp) -> next;
        lp-> next = (text *)malloc(sizeof(text));
        (lp) -> next -> elem = w;
        (lp) -> next -> next = NULL;
    }

void print_word(word *lp)
/* Печать списка из символов */
    {   
        word *op = lp;
        while (lp != NULL)
            {
                printf("%c", (lp) -> elem);
                lp = (lp) -> next;
            }
        printf(" ");
        lp = op;
    }

void delete_word(text **pilot, word *w)
/* Удаляю все слова, которые совпадают с данным */
    {   
        /* Создаю новую ссылку, чтобы уберечь основную */
        text *lp = *pilot;
        if ((lp == NULL))
            return;
        //word *op = w;
        while (((lp)) != NULL)
            {
                int flag = compare_words(lp -> elem, w);
                //printf("%d\n", flag);
                if (flag)
                    {
                        word *Dok = lp -> elem;
                        word *Dok2 = lp -> elem;
                        while ((Dok -> next != NULL) && (Dok2 -> next != NULL))
                            { 
                                if ((Dok -> next) != NULL) 
                                    Dok2 = Dok -> next;
                                free(Dok);
                                Dok = Dok2;
                            }
                        free(Dok);
                        lp -> elem = NULL;
                    }
                lp = (lp) -> next;
            }
        //lp -> elem = op;
    }

void print_text(text *lp)
/* Печать списка из списка символов */
    {
        while (lp != NULL)
            {   
                if (lp -> elem != NULL)
                    print_word(lp -> elem);
                lp = lp -> next;
            }
        printf("\n");
    }

int compare_words(word *lp1, word *lp2)
/* Сравнение двух слов */
    {
        word *op1 = lp1;
        word *op2 = lp2; 
        while ((lp1 != NULL) && (lp2 != NULL))
            {
                if ((lp1 -> elem) != (lp2 -> elem))
                    {
                        lp1 = op1;
                        lp2 = op2;
                        return 0;
                    }
                    
                lp1 = lp1 -> next;
                lp2 = lp2 -> next;
            }
        if ((lp1 == NULL) && (lp2 == NULL))
            {
                lp1 = op1;
                lp2 = op2;
                return 1;
            }
        lp1 = op1;
        lp2 = op2;
        return 0;
    }

int main(){
    
    char a;
    word *slo=NULL, *slolast = NULL;
    text *tex=NULL;
    while ((scanf("%c", &a))==1)
        {   
            if (a == ' ')
                {
                    push_text(&tex, slo);
                    slo = NULL;
                    slolast = NULL;
                }
             else
                {
                    push_word(&slo, a);
                    push_word(&slolast, a);      
                }
        }
    delete_word(&tex, slo);
    printf("\n");
    push_text(&tex, slolast);
    print_text(tex);
    return 0;
}