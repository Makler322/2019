#include <stdio.h>
#include <stdlib.h>
#include "task3_2.h"
#define SIZE 16

void start();
void word();
void greater(char l);
void greater2();
void newline();
void stop();

void nullbuf();
void addsym();
void addword();

int symset(int c);


int c; /*текущий символ */
char ** lst; /* список слов (в виде массива)*/
char * buf; /* буфер для накопления текущего слова*/
int sizebuf; /* размер буфера текущего слова*/
int sizelist; /* размер списка слов*/
int curbuf; /* индекс текущего символа в буфере*/
int curlist; /* индекс текущего слова в списке*/


void nullbuf(){
    buf=NULL;
    sizebuf=0;
    curbuf=0;
}

void addsym(){
    if (curbuf>sizebuf-1)
        buf=realloc(buf, sizebuf+=SIZE);
    buf[curbuf++]=c;
}

void addword(){
    if (curbuf>sizebuf-1)
    buf=realloc(buf, sizebuf+=1);
    buf[curbuf++]='\0';
    /*выравниваем используемую память точно по размеру слова*/
    buf=realloc(buf,sizebuf=curbuf);
    if (curlist>sizelist-1)
    lst=realloc(lst, (sizelist+=SIZE)*sizeof(*lst)); /* увеличиваем
    массив под список при необходимости */
    buf[curbuf++]='\0';
    /*выравниваем используемую память точно по размеру слова*/
    buf=realloc(buf,sizebuf=curbuf);
    if (curlist>sizelist-1)
    lst=realloc(lst, (sizelist+=SIZE)*sizeof(*lst)); /* увеличиваем
    массив под список при необходимости */
    lst[curlist++]=buf;
}
int symset(int c){
        return (c!='\n' && c!=' ' && c!='\t' && c!='>' && c!='<' && c!='&' && c!='|' && c!=',' && c!=';' && c!= EOF); 
}

void start() {
    if(c==' '|| c=='\t') { 
        c=getchar();
        if (!(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')) || (c == '/') || (c == '.') || (c == '$') || (c == '_') || (c == '/') || (c=='\n') || (c==' ') || (c=='\t') || (c=='>') || (c=='<') || (c=='&') || (c=='|') || (c==',') || (c==';') || (c== EOF))){
            printf("\nERROR (Введён неправильный символ)\n");
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            c=getchar();
            null_list();
            start();
        } 
        start();
    }
    else if (c==EOF)
    {
        termlist();
        printf("%d\n", sizelist-1);
        printlist();
        sortlist();
        printlist();
        clearlist();
        stop();
    }
    else if (c=='\n') {
        termlist();
        printf("%d", sizelist-1);
        printlist();
        sortlist();
        printlist();
        c=getchar();
        if (!(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')) || (c == '/') || (c == '.') || (c == '$') || (c == '_') || (c == '/') || (c=='\n') || (c==' ') || (c=='\t') || (c=='>') || (c=='<') || (c=='&') || (c=='|') || (c==',') || (c==';') || (c== EOF))){
            printf("\nERROR (Введён неправильный символ)\n");
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            c=getchar();
            null_list();
            start();
        }
        newline();
    }
    else if ((c=='<') || (c==',') || (c=='<')) {
        nullbuf();
        addsym();
        c=getchar();
        if (!(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')) || (c == '/') || (c == '.') || (c == '$') || (c == '_') || (c == '/') || (c=='\n') || (c==' ') || (c=='\t') || (c=='>') || (c=='<') || (c=='&') || (c=='|') || (c==',') || (c==';') || (c== EOF))){
            printf("\nERROR (Введён неправильный символ)\n");
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            c=getchar();
            null_list();
            start();
        }
        greater(' ');
    }
    else if (c=='&') {
        nullbuf();
        addsym();
        c=getchar();
        if (!(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')) || (c == '/') || (c == '.') || (c == '$') || (c == '_') || (c == '/') || (c=='\n') || (c==' ') || (c=='\t') || (c=='>') || (c=='<') || (c=='&') || (c=='|') || (c==',') || (c==';') || (c== EOF))){
            printf("\nERROR (Введён неправильный символ)\n");
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            c=getchar();
            null_list();
            start();
        }
        greater('&');
    }
    else if (c=='|') {
        nullbuf();
        addsym();
        c=getchar();
        if (!(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')) || (c == '/') || (c == '.') || (c == '$') || (c == '_') || (c == '/') || (c=='\n') || (c==' ') || (c=='\t') || (c=='>') || (c=='<') || (c=='&') || (c=='|') || (c==',') || (c==';') || (c== EOF))){
            printf("\nERROR (Введён неправильный символ)\n");
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            c=getchar();
            null_list();
            start();
        }
        greater('|');
    }
    
    else
    { 
        char cprev=c;
        nullbuf();
        addsym();
        c=getchar();
        if (!(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')) || (c == '/') || (c == '.') || (c == '$') || (c == '_') || (c == '/') || (c=='\n') || (c==' ') || (c=='\t') || (c=='>') || (c=='<') || (c=='&') || (c=='|') || (c==',') || (c==';') || (c== EOF))){
            printf("\nERROR (Введён неправильный символ)\n");
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            c=getchar();
            null_list();
            start();
        }
        (cprev=='>')? greater('>'): word();
    }
}

void word(){
    if(symset(c)) {
        addsym();
        c=getchar();
        if (!(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')) || (c == '/') || (c == '.') || (c == '$') || (c == '_') || (c == '/') || (c=='\n') || (c==' ') || (c=='\t') || (c=='>') || (c=='<') || (c=='&') || (c=='|') || (c==',') || (c==';') || (c== EOF))){
            printf("\nERROR (Введён неправильный символ)\n");
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            c=getchar();
            null_list();
            start();
        }
        word();
    }
    else
    {
        addword();
        start();
    }
}

void greater(char l) {

    if((c == l) && (l != ' ')) {
        addsym();
        c=getchar();
        if (!(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')) || (c == '/') || (c == '.') || (c == '$') || (c == '_') || (c == '/') || (c=='\n') || (c==' ') || (c=='\t') || (c=='>') || (c=='<') || (c=='&') || (c=='|') || (c==',') || (c==';') || (c== EOF))){
            printf("\nERROR (Введён неправильный символ)\n");
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            c=getchar();
            null_list();
            start();
        }
        greater2();
    }
    else
    {
        addword();
        start();
    }
}

void greater2() {
    addword();
    start();
}

void newline(){
    clearlist();
    start();
}

void stop(){
    exit(0);
}

int main(){
    c=getchar();
    if (!(((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')) || (c == '/') || (c == '.') || (c == '$') || (c == '_') || (c == '/') || (c=='\n') || (c==' ') || (c=='\t') || (c=='>') || (c=='<') || (c=='&') || (c=='|') || (c==',') || (c==';') || (c== EOF))){
            printf("\nERROR (Введён неправильный символ)\n");
            while (1){
                c=getchar();
                if ((c == '\n') || (c == EOF)) break; 
            }
            c=getchar();
            null_list();
            start();
        }
    null_list();
    start();
}