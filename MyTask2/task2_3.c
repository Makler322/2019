#include <stdio.h>
#include <time.h>

int Fibonachi_rec(int i){
    if ((i == 1) || (i == 0)){
        return i;
    }
    return Fibonachi_rec(i-1) + Fibonachi_rec(i-2);
}

int Fibonachi(int i){
    int k = 1;
    int F0 = 0;
    int F1 = 1;
    if (i==0) return 0;
    if (i==1) return 1;
    for (int j=2; j<i; j++){
        F0 = F1;
        F1 = k;
        k = F0 + F1;
    }
    return k;
}

int main(){
    int sec_do;
    int sec_posle;
    int n;
    printf("Вводите номера членов фибоначчи (не более 42 ~ 11 сек)\n");
    while (!feof(stdin)){
        scanf("%d", &n);
        sec_do = time(NULL);
        printf("Посчитали рекурсивно: %d\n",Fibonachi_rec(n));
        sec_posle = time(NULL);
        printf("Времени ушло: %d секунд\n\n", sec_posle-sec_do);
        sec_do = time(NULL);
        printf("Посчитали итерационно: %d\n",Fibonachi(n));
        sec_posle = time(NULL);
        printf("Времени ушло: %d секунд\n", sec_posle-sec_do);
    }
    return 0;
}