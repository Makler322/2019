#include <stdio.h>
#include <math.h>
double str2double(char S[]){
    int i = 0, decimal = 1, degree = 0, flag = 0, znak = 1, stepen = 1;
    double mnoj=1, Value = 0;
    while (S[i] != '\0') {
        if (S[i] == 'e'){
            i++;
            flag = 1;
            continue;
        }
        if (flag) {
            if (S[i] == '+') {
                znak = 1;
                i ++;
                continue;
            }
            if (S[i] == '-') {
                znak = 0;
                i ++;
                continue;
            }
            if ((stepen > 1) && (znak)){

                for (int j = 0; j < 9; j++){
                    mnoj*=(pow(10,(stepen - 1)));
                }
            }
            if ((stepen > 1) && (!znak)){
                for (int j = 0; j < 9; j++){
                    mnoj/=(pow(10,(stepen - 1)));
                }
            }
            if (znak) {
                for (int j = 0; j < (S[i] - '0'); j++){
                    mnoj*=10;
                }
                stepen += (S[i]-'0');
                i ++;
                continue;
            }
            if (!znak) {
                for (int j = 0; j < (S[i] - '0'); j++){
                    mnoj/=10;
                }
                stepen += (S[i]-'0');
                i ++;
                continue;
            }
        }
        if ((S[i] != '.') && (decimal)){ 
            Value *= 10;
            Value += S[i]-'0';
        // Здесь я предполагаю, что 0..9 строго упорядочены и между ними ничего нет. 
        // Иначе я воспользуюсь своей программой task1_4, она как раз на эту тему
            i++;
            continue;
        }
        if (S[i] == '.'){
            decimal = 0;
            i++;
            continue;
        }
        degree += 1;
        Value *= 10;
        Value += S[i]-'0';
        i++;
    }

    return  Value*mnoj / (pow(10, degree));
}

int main(){
    char S[25];
    printf("Вводите строки (потом они конвертируются в double): \n");
    while (scanf("%s", S)==1){
        //printf("%s", S);
        printf("%.10g\n", str2double(S));
    }
}