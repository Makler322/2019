#include <stdio.h>
#include <math.h>

double my_sqrt(double x, double eps){
    double x0 = 1;
    double x1 = (x0 + x/x0)/2;
    double x2 = (x1 + x/x1)/2;
    while (fabs(x1-x2)>eps){
        x1 = x2;
        x2 = (x1 + x/x1)/2;
    }
    return x2;
}

int main(){
    double x;
    double eps;
    printf("Введите точность:\n");
    scanf("%lf", &eps);
    printf("Вводите числа:\n");
    while(!feof(stdin)){
        scanf("%lf", &x);
        printf ("%.10g\n", my_sqrt(x, eps));
    }
    
    return 0;
}