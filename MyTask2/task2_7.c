#include <stdio.h>
#include <setjmp.h>
#include <math.h>
jmp_buf begin;

char curlex;
int flag = 0;
void getlex(void); 
int expr(void); 
int add(void); 
int mult(void); 
void error(int a);

void getlex()
    {
        while ((curlex=getchar()) == ' ');
    }

void error(int a)
    {
        printf("\nERROR!\n");
        if (a) longjmp(begin,1);
        while (getchar()!='\n');
        longjmp(begin,1);
    }

int expr()
    {
        int e=add();
        while ((curlex == '+') || (curlex == '-'))
            {
                
                if (curlex == '+')
                    {
                        getlex();
                        e+=add();
                    }
                    
                else
                    {
                        getlex();
                        e-=add();
                    }
                    
            } 
        return e;
    }

int add()
    {
        int a=mult();
        while ((curlex == '*') || (curlex == '/'))
            {
                if (curlex == '*')
                    {
                        getlex();
                        a*=mult();
                    }
                    
                else
                    {
                        getlex();
                        a/=mult();
                    }
            }
        return a;
    }
int power()
    {
        int p;
        switch(curlex)
            {
                case '0': 
                case '1': 
                case '2': 
                case '3': 
                case '4': 
                case '5':
                case '6': 
                case '7': 
                case '8': 
                case '9': p=curlex-'0'; break;
                case '(': getlex(); p=expr(); if (curlex == ')') break;
                default : error(0);
            }
        getlex();
        return p;
    }
int mult()
    {
        int m=power();
        int flag=m;
        while (curlex == '^')
            {
                getlex();
                m=pow(m, add());
            }
        
        if (!flag){
            return 0;
        }    
            
        if (m){
            return m;
        }     
            
        error(1); 
    }

int main()
    {
        int result;
        setjmp(begin);
        printf("==>");
        getlex();
        result=expr();
        if (curlex != '\n') 
            error(0);
        printf("\n%d\n",result);
        return 0;
    }