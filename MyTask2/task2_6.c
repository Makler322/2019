#include <stdio.h>
#include <stdlib.h>
typedef struct TREE { 
                        int elem;
                        struct TREE *left;
                        struct TREE *right;
                    }
        tree;

void push_tree(tree **derevo, int a)
    {
        
        if (*derevo == NULL){
            *derevo  = (tree*)malloc(sizeof(tree));
            (*derevo) -> elem = a;
            (*derevo) -> left = NULL;
            (*derevo) -> right = NULL;
            return;
        }
        tree *find = *derevo;
        while (1){
            if (a > (find -> elem)) {
                if (find -> right != NULL)
                    find = find -> right;
                else
                    {
                        find -> right = (tree *)malloc(sizeof(tree));
                        find -> right -> elem = a;
                        find -> right -> left = NULL;
                        find -> right -> right = NULL;
                        return;
                    }
                }
            if (a < (find -> elem)){
                if (find -> left != NULL)
                    find = find -> left;
                else
                    {
                        find -> left = (tree *)malloc(sizeof(tree));
                        find -> left -> elem = a;
                        find -> left -> left = NULL;
                        find -> left -> right = NULL;
                        return;
                    }
                }
        } 
    }

int find_min(tree *find){
    while (find -> left != NULL){
        find = find -> left;
    }
    return find -> elem;
}

tree *pop_tree(tree *find, int a)
    {
        if (find == NULL) return find;
        if (a > (find -> elem))
            (find -> right) = pop_tree(find -> right, a);
        else if (a < (find -> elem))
            (find -> left) = pop_tree(find -> left, a);
        else if ((find -> left != NULL) && (find -> right != NULL)){
            find -> elem = find_min(find -> right);
            (find -> right) = pop_tree(find -> right, find -> elem);
        }
        else if (find -> left != NULL)
                find = find -> left;
            else 
                find = find -> right;
        return find;
    }

int find_tree(tree **derevo, int a)
    {
        if (*derevo == NULL)
            return 0;
        tree *find = *derevo;
        while (1){
            if (a > (find -> elem)) {
                if (find -> right != NULL)
                    find = find -> right;
                else
                    return 0;
            }
            if (a < (find -> elem)){
                if (find -> left != NULL)
                    find = find -> left;
                else
                    return 0;
            }
            if (a == (find -> elem))
                return 1;
        } 
    }




int main(){
    char a;
    int n;
    tree *Derevo = NULL;
    while(scanf("%c", &a) == 1){
        if ((a == '\n') || (a == '\t') || (a == ' '))
            continue;
        scanf("%d", &n);
        if (a == '+'){
            if (!find_tree(&Derevo, n)) push_tree(&Derevo, n);
        }
        if (a == '-'){
            if (find_tree(&Derevo, n)) Derevo = pop_tree(Derevo, n);
        }
        if (a == '?'){
            if (find_tree(&Derevo, n)){
                printf("yes\n");
            }
            else{
                printf("no\n");
            }
        }
    }
    return 0;
}