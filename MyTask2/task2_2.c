#include <stdio.h>
int main(){
    double x, a, S=0;
    printf("Введите число х: \n");
    scanf("%lf", &x);
    printf("Вводите значения коэффициентов: \n");
    while(1){
        scanf("%lf", &a);
        if (feof(stdin))
            break;
        S *= x;
        S += a;
    }
    printf("Значение многочлена: %lf \n", S);
    return 0;
}