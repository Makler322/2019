#include <stdio.h>
#include <math.h>

int n = 0;
int degree;

double proizvodnya(double S, double x){
    double a, K, L;
    scanf("%lf", &a);
    if (feof(stdin)){
         degree = n;
         return 0;
    }
    n += 1;
    K = proizvodnya(S,x);
    L = (degree - n)*a*(pow(x,degree - n -1));
    n -= 1;
    return K+L ;
}

int main(){
    double x, a, S=0;
    printf("Введите число х: \n");
    scanf("%lf", &x);
    printf("Вводите значения коэффициентов: \n");
    printf("Значение производной многочлена: %lf \n", proizvodnya(S, x));
    return 0;
}