#include <stdio.h>
#include <stdlib.h>
#define SIZE 16
#include "task3_2.h"
 int c; /*текущий символ */
 char ** lst; /* список слов (в виде массива)*/
 char * buf; /* буфер для накопления текущего слова*/
 int sizebuf; /* размер буфера текущего слова*/
 int sizelist; /* размер списка слов*/
 int curbuf; /* индекс текущего символа в буфере*/
 int curlist; /* индекс текущего слова в списке*/


void clearlist(){
    int i;
    sizelist=0;
    curlist=0;
    if (lst==NULL) return;
    for (i=0; lst[i]!=NULL; i++)
        free(lst[i]);
    free(lst);
    lst=NULL;
}

void null_list(){
    sizelist=0;
    curlist=0;
    lst=NULL;
}

void termlist(){
    if (lst==NULL) return;
    if (curlist>sizelist-1)
        lst=realloc(lst,(sizelist+1)*sizeof(*lst));
    lst[curlist]=NULL;
    lst=realloc(lst,(sizelist=curlist+1)*sizeof(*lst));
}

void printlist(){
    int i;
    if (lst==NULL) return;
    for (i=0; i<sizelist-1; i++)
    printf("%s\n",lst[i]);
    printf("--------------------\n");
}

int compare(char *a, char *b){
    int i = -1;
    while (1){
        i++;
        if (a[i]>b[i]) {
            return 1;
        }
        if (a[i]<b[i]) {
            return 0;
        }
        if ((a[i]=='\0') && (b[i]!='\0')){
            return 0;
        }
        if ((a[i]!='\0') && (b[i]=='\0')){
            return 1;
        }
        if ((a[i]=='\0') && (b[i]=='\0')){
            return 1;
        }
    }

}

void sortlist(){
    int i,j;
    char *S;
    if (lst==NULL) return;
    for (i=0; i<sizelist-1; i++)
        for (j=0; j<i; j++){
             if (compare(lst[j], lst[i])){
                S = lst[i];
                lst[i] = lst[j];
                lst[j] = S;
            } 
        }
}
