/*  Воронцов А. А. */
#include "stdio.h"
#include <math.h>
int bit(int a, short int b){
    int degree = (8*b-1);
    int A = pow(2,degree), i = 0;
     for (i; i<8*b; i++){
        if (a & A) {
            printf("1"); 
        }
        else {
            printf("0");
        }
        a = a<<1;
    }
    return 0;
}

int main(){
    short int a = -1, degA = sizeof(short int), degB = sizeof(int), c = 1;
    int b = 0;
    printf("Для числа -1 расширение выглядит следующим образом: ");
    printf("\n");
    bit(a, degA);
    printf("\n");
    a += b;
    bit(a, degB);
    printf("\n");
    printf("А вот для числа 1 расширение выглядит так: ");
    printf("\n");
    bit(c, degA);
    printf("\n");
    c += b;
    bit(c, degB);
    printf("\n");
    printf("Из этого можно сделать вывод о том, что расширение от short int к int происходит знаково");
    printf("\n");
    return 0;
}