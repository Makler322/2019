#include <iostream>
#include <cstdio>
#include <iostream>

#include <stdlib.h>
#include <cmath>
#include <unistd.h>
#include <string.h>
#include <cstdlib>
using namespace std;
#include "matrix.h"
matrix::matrix (){
         n=0;
         m=0;
         table = NULL;
       }
matrix::matrix (int a, int b){
         n=b;
         m=a;
         table = new double* [m];
         for (int i=0; i<m; i++)
           table[i]= new double [n];
         for (int i=0; i<m; i++)
          for (int j=0; j<n; j++)
            table[i][j]=0;
       }
matrix::matrix (double c){
         n=1;
         m=1;
         table = new double* [m];
         for (int i=0; i<m; i++)
           table[i]= new double [n];
         for (int i=0; i<m; i++)
          for (int j=0; j<n; j++)
            table[i][j]=c;
       }
matrix::matrix (double* a, int b){
         n=b;
         m=1;
         table = new double* [m];
         for (int i=0; i<m; i++)
           table[i]= new double [n];
         for (int i=0; i<m; i++)
          for (int j=0; j<n; j++)
            table[i][j]=a[j];
       }
matrix::matrix( char* s){
	char c='s';
	int e=0;
	int f=1;
	int i=1;
	while (c!='\0') {
	   if ((c=s[i])=='{') e++;
	   i++;
	}
	c='s';
	i=1;
	while (c!='}') {
	   if ((c=s[i])==',') f++;
	   i++;
	}
	n=f;
	m=e;
      //  cout<<"N= "<<n<<"M= "<<m;
	table=new double * [m];
	int k=2;
	char * str= new char [20];
	for (int i=0; i<m; i++) {
	   table[i]=new double [n];
	   for (int j=0; j<n; j++){
	      int d=0;
	      while((s[k]!='}') && (s[k]!=',')){
	         str[d]=s[k];
	         d++;
	         k++;
	      }
	  str[d]='\0';
	  table[i][j]=atof(str);
	  if (s[k]==',') k++;
	  if (s[k]=='}') k=k+3;
	  }
	}	
	delete [] str; 
      }
matrix::matrix (int b, double* a){
         n=1;
         m=b;
         table = new double* [m];
         for (int i=0; i<m; i++)
           table[i]= new double [n];
         for (int i=0; i<m; i++)
          for (int j=0; j<n; j++)
            table[i][j]=a[i];
       }
int matrix::rows(){
       return m;
     }
     int matrix::columns(){
       return n;
     }
     void matrix::set(int i, int j, double val){
       table[i][j]=val;
     }

void matrix::print(){
         cout<<'{';
          for (int i=0; i<m; i++) {
            cout<<'{';
              for (int j=0; j<n; j++) {
               if (j!=n-1) cout<<table[i][j]<<',';
               else cout<<table[i][j];
              }
           if (i!=m-1)  cout<<"},";
           else cout<<'}';
           }
          cout<<'}';
         }
matrix::~matrix(){
       for (int i=0; i<m; i++)
        delete [] table[i];
        delete [] table;
       }
void matrix::operator = (const matrix & b){
       for (int i=0; i<m; i++)
        delete [] table[i];
        delete [] table;
        n=b.n;
        m=b.m;
        table = new double* [m];
        for (int i=0; i<m; i++)
         table[i]= new double [n];
         for (int i=0; i<m; i++)
          for (int j=0; j<n; j++)
            table[i][j]=b.table[i][j];
       
      }
void matrix::operator *= (const matrix & b){
      try{
      if (n!=b.m) {throw 3;}
      else {
       for (int i=0; i<m; i++) {
              for (int j=0; j<b.n; j++) {
                for (int k=0; k<b.n; k++)
                  {
                   table[i][j]+=(table[i][k]*(b.table[k][j]));
                  }
              }
         }
        }
       }
       catch (char *){}
      }
matrix matrix::operator * (const matrix & b){
      matrix c(m,b.n);
      try{
      if (n!=b.m) {throw 3;}
      else {
       for (int i=0; i<m; i++) {
              for (int j=0; j<b.n; j++) {
                for (int k=0; k<b.n; k++)
                  {
                   c.table[i][j]+=(table[i][k]*(b.table[k][j]));
                  }
              }
         }
        }
       }
       catch (char *){}
       return c;
      }
matrix matrix::operator | (const matrix & b){
      matrix c(m,n+b.n);
      try{
      if (m!=b.m) throw 3;
      else {
       for (int i=0; i<m; i++) 
              for (int j=0; j<n; j++) 
                   c.table[i][j]=table[i][j];
       for (int i=0; i<m; i++) 
              for (int j=n; j<n+b.n; j++) 
                   c.table[i][j]=b.table[i][j-n];
        }
       }
      catch (char *) {}
      return c;    
      }
matrix matrix::operator / (const matrix & b){
      matrix c(m+b.m,n);
      try{
      if (n!=b.n) throw 3;
      else {
       for (int i=0; i<m; i++) 
              for (int j=0; j<n; j++) 
                   c.table[i][j]=table[i][j];
       for (int i=m; i<m+b.m; i++) 
              for (int j=0; j<n; j++)
                   c.table[i][j]=b.table[i-m][j];                 
        }
       }
       catch(char *){}  
       return c;
      }
matrix  matrix::operator [] (int a){
      cout<<"M= "<<m<<"N= "<<n<<endl;
      matrix b;
      if ((a<=m) && (a>=1)) {
        matrix c(1,n);
        for (int i=0; i<n; i++) c.table[0][i]=table[a-1][i];
        b=c;
      }
      else if  ((a<=n) && (a>=1)) {
        matrix c(m,1);
        for (int i=0; i<m; i++) c.table[i][0]=table[i][a-1];
        b=c;
      }
      else {
            cout<<"ERROR! Wrong size"<<endl;
            b= *this;
           }
     return b;
     }
matrix matrix::operator * (double sc){
       matrix C(m,n);
       for (int i=0; i<m; i++) {
              for (int j=0; j<n; j++) {
                table[i][j]*=sc;
              }
       }
       C=*this;
       return C;
    }
matrix matrix::operator + (const matrix & b){
      matrix C(m,n);
      try{
          if (n!=b.n || m!=b.m)  throw 3;
          else {
          for (int i=0; i<m; i++) {
              for (int j=0; j<n; j++) {
                table[i][j]+=b.table[i][j];
                                      } 
                                  }
               }
       }
       catch(char *){}
       C=*this;
       return C;
    }
void matrix::operator += (const matrix & b){
      try{
          if (n!=b.n || m!=b.m)  throw 3;
          else {
          for (int i=0; i<m; i++) {
              for (int j=0; j<n; j++) {
                table[i][j]+=b.table[i][j];
                                      } 
                                  }
               }
       }
       catch(char *){}
    }
int matrix::operator == (const matrix & b){
     int c=1;
     try{
      if (n!=b.n || m!=b.m) throw 3;
      else {

       for (int i=0; i<m; i++) {
              for (int j=0; j<n; j++) {
                if(abs(table[i][j]-b.table[i][j])>EPS) c=0;
              }
       }
      }
     }
     catch(char *){}
     return c;
    }
int matrix::operator != (const matrix & b){
     int c=1;
     try{
      if (n!=b.n || m!=b.m) throw 3;
      else {
       for (int i=0; i<m; i++) {
              for (int j=0; j<n; j++) {
                if(abs(table[i][j]-b.table[i][j])<EPS) c=0;
              }
          }
       }
     }
     catch(char *){}
     return c;
    }
void matrix::operator -= (const matrix & b){
      try{
          if (n!=b.n || m!=b.m)  throw 3;
          else {
          for (int i=0; i<m; i++) {
              for (int j=0; j<n; j++) {
                table[i][j]-=b.table[i][j];
                                      } 
                                  }
               }
       }
       catch(char *){}
    }
matrix matrix::operator - (const matrix & b){
      matrix C(m,n);
      try{
          if (n!=b.n || m!=b.m)  throw 3;
          else {
          for (int i=0; i<m; i++) {
              for (int j=0; j<n; j++) {
                table[i][j]-=b.table[i][j];
                                      } 
                                  }
               }
       }
       catch(char *){}
       C=*this;
       return C;
    }
void matrix::operator - (){

       for (int i=0; i<m; i++) {
              for (int j=0; j<n; j++) {
               if (table[i][j]!=0) table[i][j]*=(-1);
              }
     }
    }
ostream& operator << (ostream &outs, const matrix& b){
       for (int i=0; i<b.m; i++) {
              for (int j=0; j<b.n; j++) {
                outs<<b.table[i][j]<<' ';
              }
        outs<<endl;
       }
      return outs;
    }


matrix stri (const matrix & n, int t){
         matrix b(1,n.n);
         for (int i=0; i<n.n; i++)
            b.table[0][i]=n.table[t][i];
         return b;
       }

matrix vert (const matrix & n, int t){
         matrix b(n.m,1);
         for (int i=0; i<n.m; i++)
            b.table[i][0]=n.table[i][t];
         return b;
       }