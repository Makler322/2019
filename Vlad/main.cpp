#include <iostream>
#include <cstdio>
using namespace std;
#include "matrix.h"
const double matrix::EPS=0.001;
int main(){ 
  int k,a,b;
  double c; 
  while(1){
  cout<<"Введите число, для выполнения команд:\n1 - создать матрицу MxN\n";
  cout<<"2 - создать матрицу 1х1 с заданным элементом\n3 - матрица-строка из массива\n";
  cout<<"4 - матрица-столбец из массива\n5 - матрица из строки\n6 - единичная матрица\n";
  cout<<"7 - диагональная матрица\n8 - присвоить значение\n9 - умножить единичную матрицу на скаляр\n";
  cout<<"10 - сложить матрицы\n11 - вычесть матрицы\n12 - умножить матрицы\n";
  cout<<"13 - унарный минус\n14 - сравнить матрицы\n";
  cout<<"15 - конкатенировать матрицы(верт.)\n16 - конкатенировать матрицы(гор.)\n17 - вывод строки(столбца) из ед. матрицы\n18 - выйти\n";

  cin>>k;
  switch(k){
  case 1: { 
            cout<<'\n'<<"Введите размеры: "<<endl;
            cin>>a>>b;
            matrix B(a,b);
            cout<<'\n'<<B<<endl;
            break;
           }
  case 2: {
           cout<<'\n'<<"Введите вещественное число: "<<endl;
           cin>>c;
           matrix B(c);
           cout<<'\n'<<B<<endl;
           break;
          }
  case 3: {
           cout<<'\n'<<"Введите размер массива: "<<endl;
           cin>>a;
           double * st = new double [a];
           cout<<'\n'<<"Введите числа: "<<endl;
           for (int i=0;i<a;i++) cin>>st[i];
           matrix B(st,a);
           cout<<'\n'<<B<<endl;
           delete [] st;
           break;
          }
  case 4: {
           cout<<'\n'<<"Введите размер массива: "<<endl;
           cin>>a;
           double * st = new double [a];
           cout<<'\n'<<"Введите числа: "<<endl;
           for (int i=0;i<a;i++) cin>>st[i];
           matrix B(a,st);
           cout<<'\n'<<B<<endl;
           delete [] st;
           break;
          }
  case 5: {
           cout<<'\n'<<"Введите строку: "<<endl;
           char* stri=new char [500];
           char a;
           int c=0;
           scanf("%c",&a);
           scanf("%c",&a);
           while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
           matrix B(stri);
           cout<<'\n'<<B<<endl;
           delete [] stri; 
           break;
          }
  case 6: {
           cout<<'\n'<<"Введите число (размер матрицы): "<<endl;
           cin>>b;
           cout<<'\n'<<matrix::identity(b)<<endl;
           break;
          }
  case 7: {
           cout<<'\n'<<"Введите число (размер матрицы): "<<endl;
           cin>>b;
           double st[b];
           cout<<'\n'<<"Введите числа: "<<endl;
           for (int i=0;i<b;i++) cin>>st[i];
           cout<<'\n'<<matrix::diagonal(st,b)<<endl;
           break;
          }
  case 8: {  
           cout<<'\n'<<"Введите размеры матрицы черещ пробел: "<<endl;
           cin>>a>>b;
           matrix B(a,b);
           cout<<'\n'<<"Введите номер строки, столбца (нумерация с нуля) и значение: "<<endl;
           cin>>a>>b>>c;
           B.set(a,b,c);
           cout<<'\n'<<B<<endl;
           break;
          }
  case 9: {
           cout<<'\n'<<"Введите число (размер матрицы): "<<endl;
           cin>>b;
           cout<<'\n'<<"Введите число (множитель): "<<endl;
           cin>>c;
           cout<<'\n'<<(matrix::identity(b)*c)<<endl;
           break;
          }
  case 10: {
           cout<<'\n'<<"Введите строку: "<<endl;
           char* stri=new char [500];
           char a;
           int c=0;
           scanf("%c",&a);
           scanf("%c",&a);
           while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
           matrix A(stri);
           try{
              cout<<'\n'<<"Введите строку: "<<endl;
              c=0;
              scanf("%c",&a);
              while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
             matrix B(stri);
             delete[] stri;
             A+=B;
             cout<<'\n'<<A<<endl;
            }
           catch(int){cout<<"ERROR"<<endl;}
           break;
          }
  case 11: {
           cout<<'\n'<<"Введите строку: "<<endl;
           char* stri=new char [500];
           char a;
           int c=0;
           scanf("%c",&a);
           scanf("%c",&a);
           while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
           matrix A(stri);
           try{
              cout<<'\n'<<"Введите строку: "<<endl;
              c=0;
              scanf("%c",&a);
              while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
             matrix B(stri);
             delete[] stri;
             A-=B;
             cout<<'\n'<<A<<endl;
            }
           catch(int){cout<<"ERROR"<<endl;}
           break;
          }
  case 12: {
           cout<<'\n'<<"Введите строку: "<<endl;
           char* stri=new char [500];
           char a;
           int c=0;
           scanf("%c",&a);
           scanf("%c",&a);
           while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
           matrix A(stri);
           try{
              cout<<'\n'<<"Введите строку: "<<endl;
              c=0;
              scanf("%c",&a);
              while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
             matrix B(stri);
             delete[] stri;
             cout<<'\n'<<(A*B)<<endl;
            }
           catch(int){cout<<"ERROR"<<endl;}
           break;
          }
  case 13: {
           char* stri=new char [500];
           char a;
           int c;
           try{
              cout<<'\n'<<"Введите строку: "<<endl;
              c=0;
              scanf("%c",&a);
              scanf("%c",&a);
              while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
             matrix A(stri);
             delete[] stri;
             -A;
             cout<<'\n'<<A<<endl;
            }
           catch(int){cout<<"ERROR"<<endl;}
           break;
          }
  case 14: {
           cout<<'\n'<<"Введите строку: "<<endl;
           char* stri=new char [500];
           char a;
           int c=0;
           scanf("%c",&a);
           scanf("%c",&a);
           while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
           matrix A(stri);
           try{
              cout<<'\n'<<"Введите строку: "<<endl;
              c=0;
              scanf("%c",&a);
              while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
             matrix B(stri);
             delete[] stri;
             cout<<'\n';
             if ((A==B)==0) cout<<"Не ";
             cout<<"Совпадают"<<endl;
            }
           catch(int){cout<<"Не совпадают"<<endl;}
           break;
          }
  case 15: {
           cout<<'\n'<<"Введите строку: "<<endl;
           char* stri=new char [500];
           char a;
           int c=0;
           scanf("%c",&a);
           scanf("%c",&a);
           while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
           matrix A(stri);
           try{
              cout<<'\n'<<"Введите строку: "<<endl;
              c=0;
              scanf("%c",&a);
              while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
             matrix B(stri);
             delete[] stri;
             cout<<'\n'<<(A/B)<<endl;
            }
           catch(int){cout<<"ERROR"<<endl;}
           break;
          }
  case 16: {
           cout<<'\n'<<"Введите строку: "<<endl;
           char* stri=new char [500];
           char a;
           int c=0;
           scanf("%c",&a);
           scanf("%c",&a);
           while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
           matrix A(stri);
           try{
              cout<<'\n'<<"Введите строку: "<<endl;
              c=0;
              scanf("%c",&a);
              while(a!='\n') {
              stri[c]=a;
              scanf("%c",&a);
              c++;
           }
             matrix B(stri);
             delete[] stri;
             cout<<'\n'<<(A|B)<<endl;
            }
           catch(int){cout<<"ERROR"<<endl;}
           break;
          }
  case 17: {
           matrix A(3,5);
           cout<<'\n'<<"Введите число: "<<endl;
           int a;
           cin>>a;
           cout<<'\n'<<A<<endl;
           cout<<'\n'<<A[a]<<endl;
           break;
          }
  case 18: return 0;
    }
  }  
}