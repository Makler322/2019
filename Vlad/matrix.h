#include <iostream>
#include <cstdio>
using namespace std;

class matrix {
     double **table;
     int n,m;
     static const double EPS;
   public:
     matrix ();
     matrix (int a, int b);
      matrix (double c);
      matrix (double* a, int b);
      matrix( char* s);
      matrix (int b, double* a);
     int rows();
     int columns();
     void set(int i, int j, double val);
     void print();
     ~matrix();
     
     friend ostream& operator << (ostream &outs, const matrix & b);
     static matrix identity (int k){
         matrix b(k,k);
         for (int i=0; i<k; i++)
            b.table[i][i]=1;
         return b;
       };
     static matrix diagonal (double* vals, int k){
         matrix b(k,k);
         for (int i=0; i<k; i++)
            b.table[i][i]=vals[i];
         return b;
       }
     friend matrix stri (const matrix & n, int t);
     friend matrix vert (const matrix & n, int t);
     friend matrix cut (const matrix & n, int t);
     void operator = (const matrix & b);
     void operator *= (const matrix & b);
     matrix operator * (const matrix & b);
     matrix operator | (const matrix & b);
     matrix operator / (const matrix & b);
     matrix  operator [] (int a);
     matrix operator * (double sc);
     matrix operator + (const matrix & b);
     void operator += (const matrix & b);
     int operator == (const matrix & b);
     int operator != (const matrix & b);
      void operator -= (const matrix & b);
    matrix operator - (const matrix & b);
     void operator - ();
    
 };